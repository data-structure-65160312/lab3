import java.util.Scanner;

public class SearchArray1 {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        int result = sqrt(input);
        System.out.println(result);
        
    }
    public static int sqrt(int x){
        if (x == 0 || x == 1) {
            return x;
        }

        long start = 1;
        long end = x;
        long result = 0;

        while (start <= end) {
            long mid = (start + end) / 2;
            long midSquared = mid * mid;

            if (midSquared == x) {
                return (int) mid;
            } else if (midSquared < x) {
                start = mid + 1;
                result = mid; // Store the last valid result
            } else {
                end = mid - 1;
            }
        }

        return (int) result;
    }
    }

